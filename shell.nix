let
  pkgs = import (builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/20.09.tar.gz";
  }) {};

  # To update to a newer version of easy-purescript-nix, run:
  # nix-prefetch-git https://github.com/justinwoo/easy-purescript-nix
  #
  # Then, copy the resulting rev and sha256 here.
  # Last update: 2020-09-28
  pursPkgs = import (pkgs.fetchFromGitHub {
    owner = "justinwoo";
    repo = "easy-purescript-nix";
    rev = "7b1c1635e16c7f12065db2f8ec049030fcc63655";
    sha256 = "1nybcann9aiwbvj95p6wam8xyhxwaxmfnkxmgylxcw42np2lvbzr";
  }) { inherit pkgs; };

in pkgs.stdenv.mkDerivation {
  name = "card-pool-web";
  buildInputs = with pursPkgs; [
    pursPkgs.purs
    pursPkgs.purty
    pursPkgs.spago
    pkgs.nodejs-14_x
    pkgs.heroku
    pkgs.yarn
  ];
}
