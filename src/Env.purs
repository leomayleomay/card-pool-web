module CardPool.Env
  ( Env
  , LogLevel(..)
  ) where

import Prelude

type Env
  = { logLevel :: LogLevel }

data LogLevel
  = Dev
  | Prod

derive instance eqLogLevel :: Eq LogLevel

derive instance ordLogLevel :: Ord LogLevel
