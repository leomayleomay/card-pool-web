module CardPool.Component.CardDetails where

import Prelude
import CardPool.Capability.BarcodeScanner (class BarcodeScanner, generate)
import CardPool.Component.Utils (css)
import CardPool.Data.Card (Card(..))
import Data.Const (Const)
import Data.Maybe (Maybe(..))
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE

type State
  = Card

data Action
  = Initialize
  | CloseButtonClicked

data Message
  = Return

component ::
  forall m.
  MonadAff m =>
  BarcodeScanner m =>
  H.Component HH.HTML (Const Void) Card Message m
component =
  H.mkComponent
    { initialState: identity
    , render
    , eval:
        H.mkEval
          $ H.defaultEval
              { initialize = Just Initialize
              , handleAction = handleAction
              }
    }
  where
  handleAction :: Action -> H.HalogenM State Action () Message m Unit
  handleAction = case _ of
    Initialize -> do
      Card card <- H.get
      generate card.code
    CloseButtonClicked -> H.raise Return

  render :: forall slots. State -> H.ComponentHTML Action slots m
  render card@(Card { code }) =
    HH.div
      [ css "modal is-active" ]
      [ HH.div
          [ css "modal-background" ]
          []
      , HH.div [ css "modal-content" ]
          [ HH.img [ css "barcode" ]
          , HH.img [ css "barcode" ]
          , HH.img [ css "barcode" ]
          ]
      , HH.button
          [ css "modal-close is-large"
          , HE.onClick \_ -> Just CloseButtonClicked
          ]
          []
      ]
