module CardPool.Component.Root where

import Prelude
import CardPool.Capability.BarcodeScanner (class BarcodeScanner)
import CardPool.Capability.Card (class CardService)
import CardPool.Capability.Log (class LogService)
import CardPool.Capability.Now (class Now)
import CardPool.Component.CardDetails as CardDetails
import CardPool.Component.CardList as CardList
import CardPool.Component.NewCardForm as NewCardForm
import CardPool.Component.SearchResult as SearchResult
import CardPool.Data.Card (Card)
import Data.Const (Const)
import Data.Maybe (Maybe(..))
import Data.Symbol (SProxy(..))
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH

data State
  = CardList
  | CardDetails Card
  | NewCardForm
  | SearchResult

data Action
  = HandleCardListMessage CardList.Message
  | HandleCardDetailsMessage CardDetails.Message
  | HandleNewCardFormMessage NewCardForm.Message
  | HandleSearchResultMessage SearchResult.Message

type ChildSlots
  = ( cardList :: H.Slot CardList.Query CardList.Message Unit
    , cardDetails :: H.Slot (Const Void) CardDetails.Message Unit
    , newCardForm :: H.Slot (Const Void) NewCardForm.Message Unit
    , searchResult :: H.Slot SearchResult.Query SearchResult.Message Unit
    )

component ::
  forall q i o m.
  MonadAff m =>
  Now m =>
  LogService m =>
  CardService m =>
  BarcodeScanner m =>
  H.Component HH.HTML q i o m
component =
  H.mkComponent
    { initialState: const CardList
    , render
    , eval:
        H.mkEval
          $ H.defaultEval { handleAction = handleAction }
    }
  where
  handleAction :: Action -> H.HalogenM State Action ChildSlots o m Unit
  handleAction = case _ of
    HandleCardListMessage msg -> case msg of
      CardList.NewCard -> H.modify_ $ const NewCardForm
      CardList.ViewCardDetails card -> H.modify_ $ const (CardDetails card)
    HandleCardDetailsMessage msg -> case msg of
      CardDetails.Return -> H.modify_ $ const CardList
    HandleNewCardFormMessage msg -> case msg of
      NewCardForm.Return -> H.modify_ $ const CardList
      NewCardForm.CardCreated card -> do
        H.modify_ $ const CardList
        void $ H.query (SProxy :: _ "cardList") unit $ H.tell (CardList.AddToCardList card)
    HandleSearchResultMessage msg -> case msg of
      SearchResult.Return -> H.modify_ $ const CardList
      SearchResult.AddToCardList card -> do
        H.modify_ $ const CardList
        void $ H.query (SProxy :: _ "cardList") unit $ H.tell (CardList.AddToCardList card)

  render :: State -> H.ComponentHTML Action ChildSlots m
  render = case _ of
    CardList -> HH.slot (SProxy :: _ "cardList") unit CardList.component {} $ Just <<< HandleCardListMessage
    CardDetails card -> HH.slot (SProxy :: _ "cardDetails") unit CardDetails.component card $ Just <<< HandleCardDetailsMessage
    NewCardForm -> HH.slot (SProxy :: _ "newCardForm") unit NewCardForm.component {} $ Just <<< HandleNewCardFormMessage
    SearchResult -> HH.slot (SProxy :: _ "searchResult") unit SearchResult.component {} $ Just <<< HandleSearchResultMessage
