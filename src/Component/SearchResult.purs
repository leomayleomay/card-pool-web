module CardPool.Component.SearchResult where

import Prelude
import CardPool.Data.Vendor (Vendor)
import CardPool.Capability.Card (class CardService, findCardsByVendor)
import CardPool.Component.Utils (css)
import CardPool.Data.Card (Card(..))
import Data.Maybe (Maybe(..))
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Network.RemoteData (RemoteData(..))

type State
  = RemoteData String (Array Card)

data Action
  = UserClickedAddToCardList Card
  | UserClickedReturn

data Message
  = AddToCardList Card
  | Return

data Query a
  = FindCardsByVendor Vendor a

component ::
  forall i m.
  MonadAff m =>
  CardService m =>
  H.Component HH.HTML Query i Message m
component =
  H.mkComponent
    { initialState: const NotAsked
    , render
    , eval:
        H.mkEval
          $ H.defaultEval
              { handleAction = handleAction
              , handleQuery = handleQuery
              }
    }
  where
  handleAction :: Action -> H.HalogenM State Action () Message m Unit
  handleAction = case _ of
    UserClickedReturn -> H.raise Return
    UserClickedAddToCardList product -> H.raise $ AddToCardList product

  handleQuery :: forall a. Query a -> H.HalogenM State Action () Message m (Maybe a)
  handleQuery = case _ of
    FindCardsByVendor vendor a -> do
      H.modify_ $ const Loading
      cards <- findCardsByVendor vendor
      H.modify_ $ const (Success cards)
      pure (Just a)

  renderError :: forall slots. String -> H.ComponentHTML Action slots m
  renderError error =
    HH.div
      [ css "hero is-light" ]
      [ HH.div
          [ css "hero-body" ]
          [ HH.div
              [ css "container has-text-centered" ]
              [ HH.h1
                  [ css "title has-text-danger" ]
                  [ HH.text error ]
              ]
          ]
      ]

  renderProgressBar :: forall slots. H.ComponentHTML Action slots m
  renderProgressBar =
    HH.div
      [ css "hero is-light" ]
      [ HH.div
          [ css "hero-body" ]
          [ HH.div
              [ css "container has-text-centered" ]
              [ HH.h1
                  [ css "title has-text-info" ]
                  [ HH.text "Searching cards ..." ]
              , HH.progress [ css "progress is-medium is-link", HP.max 100.0 ] []
              ]
          ]
      ]

  renderNavbar :: forall slots. H.ComponentHTML Action slots m
  renderNavbar =
    HH.nav
      [ css "navbar is-link is-fixed-top" ]
      [ HH.div
          [ css "navbar-menu" ]
          [ HH.div
              [ css "navbar-start" ]
              [ HH.a
                  [ css "navbar-item"
                  , HE.onClick $ \_ -> Just UserClickedReturn
                  ]
                  [ HH.span
                      [ css "icon is-large" ]
                      [ HH.i [ css "fas fa-lg fa-arrow-left" ] [] ]
                  ]
              ]
          ]
      ]

  renderCard :: forall slots. Card -> H.ComponentHTML Action slots m
  renderCard c@(Card card) =
    HH.div
      [ css "card" ]
      [ HH.div
          [ css "card-content" ]
          [ HH.div
              [ css "content" ]
              [ HH.p
                  [ css "title" ]
                  [ HH.text card.code ]
              , HH.p
                  [ css "sub-title" ]
                  [ HH.text $ show card.vendor ]
              ]
          ]
      , HH.footer
          [ css "card-footer" ]
          [ HH.div
              [ css "card-footer-item" ]
              [ HH.button
                  [ css "button is-fullwidth"
                  , HE.onClick $ \_ -> Just $ UserClickedAddToCardList c
                  ]
                  [ HH.span
                      [ css "icon" ]
                      [ HH.i [ css "fas fa-list-alt" ] [] ]
                  , HH.span_
                      [ HH.text "Add to card list"
                      ]
                  ]
              ]
          ]
      ]

  render :: forall slots. State -> H.ComponentHTML Action slots m
  render state =
    HH.main_
      [ renderNavbar
      , case state of
          NotAsked -> HH.text ""
          Loading -> renderProgressBar
          Failure error -> renderError error
          Success cards ->
            HH.div
              [ css "hero is-light" ]
              [ HH.div
                  [ css "hero-body" ]
                  (map renderCard cards)
              ]
      ]
