module CardPool.Component.BarcodeScanner where

import Prelude
import CardPool.Capability.BarcodeScanner (class BarcodeScanner, decodeImage, decodeVideo)
import CardPool.Capability.Log (class LogService, logError)
import CardPool.Capability.Now (class Now)
import CardPool.Component.Utils (css)
import Control.Monad.Error.Class (throwError)
import Control.Monad.Except (ExceptT)
import Control.Monad.Except.Trans (lift, runExceptT)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..), maybe)
import Data.String.Regex (Regex, regex, test)
import Data.String.Regex.Flags (ignoreCase)
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Web.File.File (toBlob)
import Web.File.FileList as FileList
import Web.File.Url (createObjectURL, revokeObjectURL)
import Web.HTML.HTMLInputElement as InputElement

data State
  = DecodingVideo
  | DecodingImage
  | DecodeVideoFailed String
  | DecodeImageFailed String

data Action
  = DecodeVideo
  | DecodeImage

data Message
  = BarcodeDetected String

data Query a
  = Decode a

notAllowedErrorRegex :: Either String Regex
notAllowedErrorRegex = regex "NotAllowedError" ignoreCase

component ::
  forall i m.
  MonadAff m =>
  Now m =>
  LogService m =>
  BarcodeScanner m =>
  H.Component HH.HTML Query i Message m
component =
  H.mkComponent
    { initialState: const DecodingVideo
    , render
    , eval:
        H.mkEval
          $ H.defaultEval
              { handleAction = handleAction
              , handleQuery = handleQuery
              }
    }
  where
  handleAction :: Action -> H.HalogenM State Action () Message m Unit
  handleAction = case _ of
    DecodeVideo -> do
      H.modify_ $ const DecodingVideo
      result <- decodeVideo
      case result of
        Left error -> H.modify_ $ const (DecodeVideoFailed error)
        Right barcode -> H.raise $ BarcodeDetected barcode
    DecodeImage -> do
      rFile <-
        runExceptT do
          h <- onNothing "input not found" =<< (lift <<< H.getHTMLElementRef $ H.RefLabel "input")
          i <- onNothing "cannot convert input" $ InputElement.fromHTMLElement h
          f <- onNothing "no file found" =<< (H.liftEffect $ InputElement.files i)
          onNothing "files was empty" $ FileList.item 0 f
      case rFile of
        Left error -> logError error
        Right file -> do
          H.modify_ $ const DecodingImage
          objectURL <- H.liftEffect $ createObjectURL $ toBlob file
          rBarcode <- decodeImage objectURL
          case rBarcode of
            Left error -> H.modify_ $ const (DecodeImageFailed error)
            Right barcode -> H.raise $ BarcodeDetected barcode
          H.liftEffect $ revokeObjectURL objectURL
    where
    onNothing :: forall m' a. Monad m' => String -> Maybe a -> ExceptT String m' a
    onNothing e = maybe (throwError e) pure

  handleQuery :: forall a. Query a -> H.HalogenM State Action () Message m (Maybe a)
  handleQuery = case _ of
    Decode a -> do
      handleAction DecodeVideo
      pure (Just a)

  renderError :: forall slots. String -> H.ComponentHTML Action slots m
  renderError error =
    HH.div
      [ css "hero is-light" ]
      [ HH.div
          [ css "hero-body" ]
          [ HH.div
              [ css "container has-text-centered" ]
              [ HH.h1
                  [ css "title has-text-danger" ]
                  [ HH.text error ]
              ]
          ]
      ]

  renderProgressBar :: forall slots. H.ComponentHTML Action slots m
  renderProgressBar =
    HH.div
      [ css "hero is-light" ]
      [ HH.div
          [ css "hero-body" ]
          [ HH.div
              [ css "container has-text-centered" ]
              [ HH.h1
                  [ css "title has-text-info" ]
                  [ HH.text "Detecing barcode ..." ]
              , HH.progress [ css "progress is-medium is-link", HP.max 100.0 ] []
              ]
          ]
      ]

  renderDecodeVideoButton :: forall slots. H.ComponentHTML Action slots m
  renderDecodeVideoButton =
    HH.div
      [ css "has-text-centered" ]
      [ HH.button
          [ css "button is-large"
          , HE.onClick $ \_ -> Just DecodeVideo
          ]
          [ HH.span
              [ css "icon is-large" ]
              [ HH.i [ css "fas fa-lg fa-barcode" ] [] ]
          , HH.span_
              [ HH.text
                  "Scan another barcode"
              ]
          ]
      ]

  renderDecodeImageButton :: forall slots. H.ComponentHTML Action slots m
  renderDecodeImageButton =
    HH.div
      [ css ("file is-boxed is-large") ]
      [ HH.label
          [ css "file-label" ]
          [ HH.input
              [ HP.type_ HP.InputFile
              , HP.ref (H.RefLabel "input")
              , css "file-input"
              , HP.attr (HH.AttrName "accept") "image/*"
              , HP.attr (HH.AttrName "capture") "camera"
              , HE.onChange $ \_ -> Just DecodeImage
              ]
          , HH.span
              [ css "file-cta" ]
              [ HH.span
                  [ css "file-icon is-large" ]
                  [ HH.i [ css "fas fa-lg fa-barcode" ] [] ]
              , HH.span
                  [ css "file-label" ]
                  [ HH.text "Take a picture of barcode" ]
              ]
          ]
      ]

  render :: forall slots. State -> H.ComponentHTML Action slots m
  render = case _ of
    DecodingVideo -> HH.video [ HP.id_ "video" ] []
    DecodeVideoFailed error -> case notAllowedErrorRegex of
      Left _ ->
        HH.div_
          [ renderError error
          , renderDecodeVideoButton
          ]
      Right regex ->
        if test regex error then
          HH.div_
            [ renderDecodeImageButton
            ]
        else
          HH.div_
            [ renderError error
            , renderDecodeVideoButton
            ]
    DecodingImage ->
      HH.div_
        [ renderProgressBar
        ]
    DecodeImageFailed error ->
      HH.div_
        [ renderError error
        , renderDecodeImageButton
        ]
