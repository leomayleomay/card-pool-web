module CardPool.Component.CardList where

import Prelude
import CardPool.Capability.Card (class CardService, readFromLocal, writeToLocal)
import CardPool.Component.Utils (css)
import CardPool.Data.Card (Card(..))
import CardPool.Data.Vendor (Vendor(..))
import Data.Array (elem, delete, snoc, elemIndex, updateAt, length, filter)
import Data.Maybe (Maybe(..))
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Web.HTML (window)
import Web.HTML.Window (confirm)

data Filter
  = Favorite
  | All

derive instance eqFilter :: Eq Filter

type State
  = { cards :: Array Card
    , currentFilter :: Filter
    }

data Action
  = Initialize
  | FilterUpdated Filter
  | NewButtonClicked
  | UserClickedCard Card
  | UserToggledCard Card
  | UserDeletedCard Card
  | Finalize

data Message
  = NewCard
  | ViewCardDetails Card

data Query a
  = AddToCardList Card a

component ::
  forall i m.
  MonadAff m =>
  CardService m =>
  H.Component HH.HTML Query i Message m
component =
  H.mkComponent
    { initialState: const { cards: [], currentFilter: All }
    , render
    , eval:
        H.mkEval
          $ H.defaultEval
              { initialize = Just Initialize
              , handleAction = handleAction
              , handleQuery = handleQuery
              , finalize = Just Finalize
              }
    }
  where
  handleAction :: Action -> H.HalogenM State Action () Message m Unit
  handleAction = case _ of
    Initialize -> do
      cards <- readFromLocal
      let
        favorites = filter (\(Card { favorite }) -> favorite) cards
      if length favorites > 0 then
        H.modify_ _ { cards = cards, currentFilter = Favorite }
      else
        H.modify_ _ { cards = cards }
    FilterUpdated filter -> H.modify_ _ { currentFilter = filter }
    NewButtonClicked -> H.raise NewCard
    UserClickedCard card -> H.raise $ ViewCardDetails card
    UserToggledCard card@(Card c@{ favorite }) -> do
      { cards } <- H.get
      let
        mNewCards = do
          index <- elemIndex card cards
          updateAt index (Card (c { favorite = not favorite })) cards
      case mNewCards of
        Nothing -> pure unit
        Just newCards -> do
          H.modify_ _ { cards = newCards }
          writeToLocal newCards
    UserDeletedCard card -> do
      confirmed <-
        H.liftEffect
          $ confirm "Are you sure?"
          =<< window
      when (confirmed) do
        { cards } <- H.get
        let
          newCards = delete card cards
        H.modify_ _ { cards = newCards }
        writeToLocal newCards
    Finalize -> do
      { cards } <- H.get
      writeToLocal cards

  handleQuery :: forall a slots. Query a -> H.HalogenM State Action slots Message m (Maybe a)
  handleQuery = case _ of
    AddToCardList card a -> do
      { cards } <- H.get
      unless (elem card cards) do
        let
          newCards = snoc cards card
        H.modify_ _ { cards = newCards }
        writeToLocal newCards
      pure (Just a)

  renderCard :: forall slots. Card -> H.ComponentHTML Action slots m
  renderCard card@(Card { notes, vendor, favorite }) =
    HH.a
      [ css "panel-block is-block" ]
      [ HH.div
          [ css "card" ]
          [ HH.div
              [ css "card-image", HE.onClick $ \_ -> Just $ UserClickedCard card ]
              [ HH.figure
                  [ css "image" ]
                  [ HH.img
                      [ HP.src
                          $ case vendor of
                              Countdown -> "/cards/onecard.png"
                              NewWorld -> "/cards/newworld.png"
                      ]
                  ]
              ]
          , case notes of
              Nothing -> HH.text ""
              Just notes' ->
                HH.div
                  [ css "card-content" ]
                  [ HH.div
                      [ css "content" ]
                      [ HH.text notes' ]
                  ]
          , HH.footer
              [ css "card-footer" ]
              [ HH.a
                  [ css "card-footer-item", HE.onClick \_ -> Just $ UserToggledCard card ]
                  [ if favorite then
                      HH.span
                        [ css "icon" ]
                        [ HH.i [ css "fas fa-star" ] [] ]
                    else
                      HH.span
                        [ css "icon is-large" ]
                        [ HH.i [ css "far fa-star" ] [] ]
                  ]
              , HH.a
                  [ css "card-footer-item", HE.onClick $ \_ -> Just $ UserDeletedCard card ]
                  [ HH.span
                      [ css "icon" ]
                      [ HH.i [ css "far fa-lg fa-times-circle" ] [] ]
                  ]
              ]
          ]
      ]

  renderNavbar :: forall slots. H.ComponentHTML Action slots m
  renderNavbar =
    HH.nav
      [ css "navbar is-link is-fixed-top" ]
      [ HH.div
          [ css "navbar-menu" ]
          [ HH.div
              [ css "navbar-item" ]
              [ HH.a
                  [ css "button", HE.onClick $ \_ -> Just NewButtonClicked ]
                  [ HH.span
                      [ css "icon is-large" ]
                      [ HH.i [ css "fas fa-lg fa-plus" ] [] ]
                  ]
              ]
          ]
      ]

  render :: forall slots. State -> H.ComponentHTML Action slots m
  render { cards, currentFilter } =
    HH.main_
      [ renderNavbar
      , HH.div
          [ css "panel" ]
          [ HH.p
              [ css "panel-tabs" ]
              [ HH.a
                  [ if (currentFilter == Favorite) then
                      css "is-active"
                    else
                      HE.onClick $ \_ -> Just $ FilterUpdated Favorite
                  ]
                  [ HH.text $ "Favorite (" <> (show $ length favorites) <> ")" ]
              , HH.a
                  [ if (currentFilter == All) then
                      css "is-active"
                    else
                      HE.onClick $ \_ -> Just $ FilterUpdated All
                  ]
                  [ HH.text $ "All (" <> (show $ length cards) <> ")" ]
              ]
          , case currentFilter of
              Favorite ->
                HH.div_
                  $ renderCard
                  <$> favorites
              All ->
                HH.div_
                  $ renderCard
                  <$> cards
          ]
      , if length cards == 0 then
          HH.section
            [ css "hero is-medium" ]
            [ HH.div
                [ css "hero-body" ]
                [ HH.div
                    [ css "container has-text-centered" ]
                    [ HH.h1
                        [ css "subtitle" ]
                        [ HH.div
                            [ css "columns is-vcentered is-mobile" ]
                            [ HH.div
                                [ css "column is-12" ]
                                [ HH.button
                                    [ css "button", HE.onClick $ \_ -> Just NewButtonClicked ]
                                    [ HH.span
                                        [ css "icon" ]
                                        [ HH.i [ css "fas fa-plus" ] [] ]
                                    , HH.span_
                                        [ HH.text "Add a new card!" ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        else
          HH.text ""
      ]
    where
    favorites = filter (\(Card { favorite }) -> favorite) cards
