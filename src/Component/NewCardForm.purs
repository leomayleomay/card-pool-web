module CardPool.Component.NewCardForm where

import Prelude
import CardPool.Capability.BarcodeScanner (class BarcodeScanner, resetCodeReader)
import CardPool.Capability.Log (class LogService)
import CardPool.Capability.Now (class Now)
import CardPool.Component.BarcodeScanner as BarcodeScanner
import CardPool.Component.Utils (css)
import CardPool.Data.Card (Card(..))
import CardPool.Data.Source (Source(..))
import CardPool.Data.Vendor (Vendor(..))
import Data.Const (Const)
import Data.Maybe (Maybe(..), fromJust, isNothing)
import Data.String.Read (read)
import Data.Symbol (SProxy(..))
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Partial.Unsafe (unsafePartial)

type ChildSlots
  = ( barcodeScanner :: H.Slot BarcodeScanner.Query BarcodeScanner.Message Unit
    )

type State
  = { code :: Maybe String
    , vendor :: Maybe Vendor
    , notes :: Maybe String
    }

data Action
  = Initialize
  | UserClickedReturn
  | NotesUpdated String
  | VendorUpdated String
  | CreateButtonClicked
  | HandleBarcodeScannerMessage BarcodeScanner.Message

data Message
  = Return
  | CardCreated Card

component ::
  forall i m.
  MonadAff m =>
  Now m =>
  LogService m =>
  BarcodeScanner m =>
  H.Component HH.HTML (Const Void) i Message m
component =
  H.mkComponent
    { initialState: const { code: Nothing, vendor: Nothing, notes: Nothing }
    , render
    , eval:
        H.mkEval
          $ H.defaultEval
              { initialize = Just Initialize
              , handleAction = handleAction
              }
    }
  where
  handleAction :: Action -> H.HalogenM State Action ChildSlots Message m Unit
  handleAction = case _ of
    Initialize -> void $ H.query (SProxy :: _ "barcodeScanner") unit $ H.tell (BarcodeScanner.Decode)
    UserClickedReturn -> do
      resetCodeReader
      H.raise Return
    VendorUpdated vendor -> H.modify_ _ { vendor = read vendor }
    NotesUpdated notes -> H.modify_ _ { notes = Just notes }
    CreateButtonClicked -> do
      { code, vendor, notes } <- H.get
      case code of
        Nothing -> pure unit
        Just code' -> H.raise $ CardCreated (Card { code: code', vendor: unsafePartial $ fromJust vendor, notes, source: Me, favorite: false })
    HandleBarcodeScannerMessage msg -> case msg of
      BarcodeScanner.BarcodeDetected barcode -> H.modify_ _ { code = Just barcode }

  renderNavbar :: forall slots. H.ComponentHTML Action slots m
  renderNavbar =
    HH.nav
      [ css "navbar is-link is-fixed-top" ]
      [ HH.div
          [ css "navbar-menu" ]
          [ HH.div
              [ css "navbar-start" ]
              [ HH.a
                  [ css "navbar-item"
                  , HE.onClick $ \_ -> Just UserClickedReturn
                  ]
                  [ HH.span
                      [ css "icon is-large" ]
                      [ HH.i [ css "fas fa-lg fa-arrow-left" ] [] ]
                  ]
              ]
          ]
      ]

  render :: State -> H.ComponentHTML Action ChildSlots m
  render form@{ code, vendor, notes } =
    HH.main_
      [ renderNavbar
      , HH.div
          [ css "hero is-light" ]
          [ HH.div
              [ css "hero-body" ]
              [ case code of
                  Nothing ->
                    HH.div
                      [ css "field" ]
                      [ HH.label
                          [ css "label" ]
                          [ HH.text "Scan barcode" ]
                      , HH.div
                          [ css "control" ]
                          [ HH.slot (SProxy :: _ "barcodeScanner") unit BarcodeScanner.component {} $ Just <<< HandleBarcodeScannerMessage ]
                      ]
                  Just code' ->
                    HH.div
                      [ css "field" ]
                      [ HH.label
                          [ css "label" ]
                          [ HH.text "Card Number" ]
                      , HH.div
                          [ css "control" ]
                          [ HH.input
                              [ css "input is-fullwidth"
                              , HP.readOnly true
                              , HP.value code'
                              ]
                          ]
                      ]
              , HH.div
                  [ css "field" ]
                  [ HH.label
                      [ css "label" ]
                      [ HH.text "Card Type" ]
                  , HH.div
                      [ css "control" ]
                      [ HH.div
                          [ css "select is-fullwidth" ]
                          [ HH.select
                              [ HE.onValueChange $ Just <<< VendorUpdated ]
                              [ HH.option_
                                  [ HH.text "-- Please select --" ]
                              , HH.option
                                  [ HP.value "Countdown" ]
                                  [ HH.text "Countdown" ]
                              , HH.option
                                  [ HP.value "NewWorld" ]
                                  [ HH.text "NewWorld" ]
                              ]
                          ]
                      ]
                  ]
              , HH.div
                  [ css "field" ]
                  [ case vendor of
                      Nothing -> HH.text ""
                      Just NewWorld ->
                        HH.figure
                          [ css "image" ]
                          [ HH.img [ HP.src "/cards/newworld.png" ]
                          ]
                      Just Countdown ->
                        HH.figure
                          [ css "image" ]
                          [ HH.img [ HP.src "/cards/onecard.png" ]
                          ]
                  ]
              , HH.div
                  [ css "field" ]
                  [ HH.label
                      [ css "label" ]
                      [ HH.text "Notes" ]
                  , HH.div
                      [ css "control" ]
                      [ HH.textarea
                          [ css "input"
                          , HP.rows 3
                          , HE.onValueInput $ Just <<< NotesUpdated
                          ]
                      ]
                  ]
              , HH.div
                  [ css "field" ]
                  [ HH.div
                      [ css "control" ]
                      [ HH.button
                          [ css "button is-link is-fullwidth"
                          , HE.onClick \_ -> Just CreateButtonClicked
                          , HP.disabled $ (isNothing vendor || isNothing code)
                          ]
                          [ HH.text "Add" ]
                      ]
                  ]
              ]
          ]
      ]
