module CardPool.Capability.Card where

import Prelude
import CardPool.Data.Card (Card)
import CardPool.Data.Vendor (Vendor)
import Control.Monad.Trans.Class (lift)
import Halogen (HalogenM)

class
  Monad m <= CardService m where
  findCardsByVendor :: Vendor -> m (Array Card)
  readFromLocal :: m (Array Card)
  writeToLocal :: (Array Card) -> m Unit

instance cardServiceHalogenM :: CardService m => CardService (HalogenM st act slots msg m) where
  findCardsByVendor = lift <<< findCardsByVendor
  readFromLocal  = lift readFromLocal
  writeToLocal = lift <<< writeToLocal
