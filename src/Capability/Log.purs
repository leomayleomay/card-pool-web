module CardPool.Capability.Log where

import Prelude
import CardPool.Capability.Now (class Now)
import CardPool.Data.Log (Log, LogReason(..), mkLog)
import Control.Monad.Trans.Class (lift)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Halogen (HalogenM)

class
  Monad m <= LogService m where
  log :: Log -> m Unit

instance logServiceHalogenM :: LogService m => LogService (HalogenM st act slots msg m) where
  log = lift <<< log

logDebug :: forall m. LogService m => Now m => String -> m Unit
logDebug = log <=< mkLog Debug

logInfo :: forall m. LogService m => Now m => String -> m Unit
logInfo = log <=< mkLog Info

logWarn :: forall m. LogService m => Now m => String -> m Unit
logWarn = log <=< mkLog Warn

logError :: forall m. LogService m => Now m => String -> m Unit
logError = log <=< mkLog Error

logHush :: forall m a. LogService m => Now m => LogReason -> m (Either String a) -> m (Maybe a)
logHush reason action =
  action
    >>= case _ of
        Left e -> case reason of
          Debug -> logDebug e *> pure Nothing
          Info -> logInfo e *> pure Nothing
          Warn -> logWarn e *> pure Nothing
          Error -> logError e *> pure Nothing
        Right v -> pure $ Just v

debugHush :: forall m a. LogService m => Now m => m (Either String a) -> m (Maybe a)
debugHush = logHush Debug
