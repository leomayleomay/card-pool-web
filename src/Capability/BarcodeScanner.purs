module CardPool.Capability.BarcodeScanner where

import Prelude
import Control.Monad.Trans.Class (lift)
import Data.Either (Either)
import Halogen (HalogenM)

class
  Monad m <= BarcodeScanner m where
  generate :: String -> m Unit
  resetCodeReader :: m Unit
  decodeVideo :: m (Either String String)
  decodeImage :: String -> m (Either String String)

instance barcodeScannerHalogenM :: BarcodeScanner m => BarcodeScanner (HalogenM st act slots msg m) where
  generate = lift <<< generate
  resetCodeReader = lift resetCodeReader
  decodeVideo = lift decodeVideo
  decodeImage = lift <<< decodeImage
