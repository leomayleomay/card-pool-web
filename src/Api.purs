module CardPool.Api where

import Prelude
import Affjax (request)
import Affjax.ResponseFormat as ResponseFormat
import CardPool.Data.Card (Card(..))
import CardPool.Data.Vendor (Vendor)
import Data.Argonaut.Core (Json)
import Data.Argonaut.Decode (decodeJson, (.:), (.:?))
import Data.Bifunctor (rmap)
import Data.Either (Either(..), hush, either)
import Data.HTTP.Method (Method(..))
import Data.Maybe (Maybe(..), maybe)
import Data.Traversable (for)
import Effect.Aff.Class (class MonadAff, liftAff)

decode :: forall m a. MonadAff m => (Json -> Either String a) -> Maybe Json -> m (Maybe a)
decode decoder json = pure $ (hush <<< decoder) =<< json

decodeArray :: forall m a. MonadAff m => (Json -> Either String (Array a)) -> Maybe Json -> m (Array a)
decodeArray decoder mJson = pure $ maybe [] (\json -> either (const []) identity $ decoder json) mJson

findCardsByVendor ::
  forall m.
  MonadAff m =>
  Vendor -> m (Array Card)
findCardsByVendor vendor = do
  response <-
    liftAff $ request
      $ { method: Left GET
        , url: "https://card-pool-api.herokuapp.com/cards?vendor=" <> show vendor
        , headers: []
        , content: Nothing
        , username: Nothing
        , password: Nothing
        , withCredentials: false
        , responseFormat: ResponseFormat.json
        }
  let
    mJson = hush $ rmap _.body response
  result <- decodeArray decodeCards mJson
  pure result

decodeCards :: Json -> Either String (Array Card)
decodeCards json = do
  cards <- decodeJson json
  for cards \p -> do
    code <- p .: "code"
    notes <- p .:? "notes"
    vendor <- p .: "vendor"
    source <- p .: "source"
    Right $ Card { code, notes, vendor, source, favorite: false }
