module Main where

import Prelude
import CardPool.AppM (runAppM)
import CardPool.Component.Root as Root
import CardPool.Env (Env, LogLevel(..))
import Effect (Effect)
import Effect.Aff (Aff)
import Halogen as H
import Halogen.Aff as HA
import Halogen.HTML as HH
import Halogen.VDom.Driver (runUI)

main :: Effect Unit
main =
  HA.runHalogenAff do
    body <- HA.awaitBody
    let
      environment :: Env
      environment =
        { logLevel: Dev
        }

      rootComponent :: forall q. H.Component HH.HTML q {} Void Aff
      rootComponent = H.hoist (runAppM environment) Root.component
    runUI rootComponent {} body
