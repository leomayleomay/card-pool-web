module CardPool.Data.Card where

import Prelude
import CardPool.Data.Vendor (Vendor)
import CardPool.Data.Source (Source)
import Data.Argonaut.Decode (class DecodeJson)
import Data.Argonaut.Encode (class EncodeJson)
import Data.Newtype (class Newtype)
import Data.Maybe (Maybe)

newtype Card
  = Card
  { code :: String
  , vendor :: Vendor
  , source :: Source
  , favorite :: Boolean
  , notes :: Maybe String
  }

derive instance newtypeCard :: Newtype Card _

derive newtype instance encodeJsonCard :: EncodeJson Card

derive newtype instance decodeJsonCard :: DecodeJson Card

instance eqCard :: Eq Card where
  eq (Card c1) (Card c2) = c1.code == c2.code && c1.vendor == c2.vendor
