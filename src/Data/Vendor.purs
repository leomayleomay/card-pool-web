module CardPool.Data.Vendor where

import Prelude
import Data.Argonaut.Decode (class DecodeJson, decodeJson)
import Data.Argonaut.Encode (class EncodeJson, encodeJson)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.String.Read (class Read)

data Vendor
  = NewWorld
  | Countdown

derive instance eqVendor :: Eq Vendor

derive instance ordVendor :: Ord Vendor

instance decodeJsonVendor :: DecodeJson Vendor where
  decodeJson =
    decodeJson
      >=> case _ of
          "NewWorld" -> pure NewWorld
          "Countdown" -> pure Countdown
          _ -> Left "Unknown vendor"

instance encodeJsonVendor :: EncodeJson Vendor where
  encodeJson NewWorld = encodeJson "NewWorld"
  encodeJson Countdown = encodeJson "Countdown"

instance showVendor :: Show Vendor where
  show NewWorld = "New World"
  show Countdown = "Countdown"

instance readVendor :: Read Vendor where
  read "NewWorld" = Just NewWorld
  read "Countdown" = Just Countdown
  read _ = Nothing
