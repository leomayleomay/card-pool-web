module CardPool.Data.Source where

import Prelude
import Data.Maybe (Maybe)
import Data.Either (Either(Left))
import Data.Argonaut.Decode (class DecodeJson, decodeJson)
import Data.Argonaut.Encode (class EncodeJson, encodeJson)

data Source
  = Me
  | Friend
  | Others

derive instance eqSource :: Eq Source

derive instance ordSource :: Ord Source

instance decodeJsonSource :: DecodeJson Source where
  decodeJson =
    decodeJson
      >=> case _ of
          "me" -> pure Me
          "friend" -> pure Friend
          "others" -> pure Others
          _ -> Left "Unknown source"

instance encodeJsonSource :: EncodeJson Source where
  encodeJson Me = encodeJson "me"
  encodeJson Friend = encodeJson "Friend"
  encodeJson Others = encodeJson "Others"

instance showSource :: Show Source where
  show Me = "me"
  show Friend = "friend"
  show Others = "others"
