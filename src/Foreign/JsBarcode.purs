module CardPool.Foreign.JsBarcode where

import Prelude
import Effect (Effect)

foreign import generate :: String -> Effect Unit
