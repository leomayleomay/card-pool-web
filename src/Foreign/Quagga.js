"use strict";

const Quagga = window.Quagga;

exports.decodeImpl = function (image) {
  return function (size) {
    return function (onError, onSuccess) {
      Quagga.decodeSingle({
        src: image,
        inputStream: {
          size: size,
          singleChannel: false
        },
        decoder: {
          readers: ["ean_reader", "ean_8_reader", "upc_reader", "upc_e_reader"]
        }
      }, function (result) {
        if (result && result.codeResult) {
          onSuccess(result.codeResult.code);
        } else {
          onError('No barcode found in picture');
        }
      });

      return function (cancelError, cancelerError, cancelerSuccess) {
        cancelerSuccess();
      };
    }
  }
}