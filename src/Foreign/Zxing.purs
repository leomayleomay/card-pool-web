module CardPool.Foreign.Zxing where

import Prelude
import Effect (Effect)
import Effect.Aff (Aff)
import Effect.Aff.Compat (EffectFnAff, fromEffectFnAff)

foreign import resetCodeReader :: Effect Unit

foreign import decodeImageImpl :: String -> EffectFnAff String

decodeImage :: String -> Aff String
decodeImage = fromEffectFnAff <<< decodeImageImpl

foreign import decodeVideoImpl :: EffectFnAff String

decodeVideo :: Aff String
decodeVideo = fromEffectFnAff decodeVideoImpl
