"use strict";

exports.resetCodeReader = function () {
  if (window.codeReader) {
    window.codeReader.reset();
  }
};

function initCodeReader() {
  if (window.codeReader) {
    return window.codeReader;
  } else {
    window.codeReader = new ZXing.BrowserBarcodeReader();

    return window.codeReader;
  }
}

exports.decodeImageImpl = function (image) {
  return function (onError, onSuccess) {
    const codeReader = initCodeReader();

    codeReader.decodeFromImage(undefined, image).then(function (result) {
      codeReader.reset();

      onSuccess(result.text);
    }).catch(onError);

    return function (cancelError, cancelerError, cancelerSuccess) {
      cancelerSuccess();
    };
  }
};

exports.decodeVideoImpl = function (onError, onSuccess) {
  const codeReader = initCodeReader();

  codeReader.decodeOnceFromVideoDevice(undefined, 'video').then(function (result) {
    codeReader.reset();

    onSuccess(result.text);
  }).catch(onError);

  return function (cancelError, cancelerError, cancelerSuccess) {
    cancelerSuccess();
  };
}
