"use strict";

const JsBarcode = require('jsbarcode');

exports.generate = function (barcode) {
  return function () {
    JsBarcode(".barcode", barcode, {
      displayValue: false
    });

    return;
  }
};
