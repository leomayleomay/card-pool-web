module CardPool.Foreign.Quagga where

import Prelude
import Effect.Aff (Aff)
import Effect.Aff.Compat (EffectFnAff, fromEffectFnAff)

foreign import decodeImpl :: String -> Int -> EffectFnAff String

decode :: String -> Int -> Aff String
decode image size = fromEffectFnAff $ decodeImpl image size
