module CardPool.AppM where

import Prelude
import CardPool.Api as Api
import CardPool.Capability.BarcodeScanner (class BarcodeScanner)
import CardPool.Capability.Card (class CardService)
import CardPool.Capability.Log (class LogService)
import CardPool.Capability.Now (class Now)
import CardPool.Data.Log as Log
import CardPool.Env (Env, LogLevel(..))
import CardPool.Foreign.JsBarcode as JsBarcode
import CardPool.Foreign.Quagga as Quagga
import CardPool.Foreign.Zxing as Zxing
import Control.Alt ((<|>))
import Control.Monad.Error.Class (try)
import Control.Monad.Reader.Trans (class MonadAsk, ReaderT, ask, asks, runReaderT)
import Data.Argonaut.Core (stringify)
import Data.Argonaut.Decode (decodeJson)
import Data.Argonaut.Encode (encodeJson)
import Data.Argonaut.Parser (jsonParser)
import Data.Bifunctor (lmap)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Effect.Aff (Aff)
import Effect.Aff.Class (class MonadAff, liftAff)
import Effect.Class (class MonadEffect, liftEffect)
import Effect.Console as Console
import Effect.Now as Now
import Type.Equality (class TypeEquals, from)
import Web.HTML (window)
import Web.HTML.Window (localStorage)
import Web.Storage.Storage (getItem, setItem)

newtype AppM a
  = AppM (ReaderT Env Aff a)

runAppM :: Env -> AppM ~> Aff
runAppM env (AppM m) = runReaderT m env

derive newtype instance functorAppM :: Functor AppM

derive newtype instance applyAppM :: Apply AppM

derive newtype instance applicativeAppM :: Applicative AppM

derive newtype instance bindAppM :: Bind AppM

derive newtype instance monadAppM :: Monad AppM

derive newtype instance monadEffectAppM :: MonadEffect AppM

derive newtype instance monadAffAppM :: MonadAff AppM

instance monadAskAppM :: TypeEquals e Env => MonadAsk e AppM where
  ask = AppM $ asks from

instance nowAppM :: Now AppM where
  nowDateTime = liftEffect Now.nowDateTime

instance logServiceAppM :: LogService AppM where
  log message = do
    { logLevel } <- ask
    liftEffect case logLevel, Log.reason message of
      Prod, Log.Debug -> pure unit
      _, _ -> Console.log $ Log.message message

instance cardServiceAppM :: CardService AppM where
  findCardsByVendor vendor = Api.findCardsByVendor vendor
  readFromLocal = do
    mbStr <- liftEffect $ getItem "card-pool" =<< localStorage =<< window
    case mbStr of
      Nothing -> pure []
      Just str -> do
        case decodeJson =<< jsonParser str of
          Left _ -> pure []
          Right cards -> pure cards
  writeToLocal cards = do
    let
      str = stringify $ encodeJson cards
    liftEffect $ setItem "card-pool" str =<< localStorage =<< window

instance barcodeScannerAppM :: BarcodeScanner AppM where
  generate code = liftEffect $ JsBarcode.generate code
  resetCodeReader = liftEffect $ Zxing.resetCodeReader
  decodeVideo = do
    result <-
      liftAff $ try
        $ Zxing.decodeVideo
    pure $ lmap show result
  decodeImage image = do
    result <-
      liftAff $ try
        $ Zxing.decodeImage image
        <|> Quagga.decode image 1920
        <|> Quagga.decode image 1600
        <|> Quagga.decode image 1280
        <|> Quagga.decode image 800
        <|> Quagga.decode image 640
        <|> Quagga.decode image 320
    pure $ lmap show result
